def serialize(user):

    serialized_user = serialize_id(user)

    serialized_user.update({
        'username': user.username,
        'email': user.email,
        'is_confirmed': user.is_confirmed,
        'token': user.token
    })

    return serialized_user


def serialize_id(user):
    return {
        'id': user.id
    }
