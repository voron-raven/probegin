import jwt

from django.conf import settings
from django.contrib.auth import get_user_model
from utils.api.exceptions import (
    RequestValidationFailedAPIError,
    RequestDecodeFailedAPIError
)

User = get_user_model()


class JWTAuthentication(object):
    def authenticate(self, token):
        """
        Try to authenticate the given credentials. If authentication is
        successful, return the user and token. If not, throw an error.
        """
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
        except:
            msg = 'Invalid authentication. Could not decode token.'
            raise RequestDecodeFailedAPIError(msg)

        try:
            user = User.objects.get(pk=payload['id'])
        except User.DoesNotExist:
            msg = 'No user matching this token was found.'
            # todo: Need specific error.
            raise RequestValidationFailedAPIError(msg)

        if not user.is_active:
            msg = 'This user has been deactivated.'
            # todo: Need specific error.
            raise RequestValidationFailedAPIError(msg)

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
