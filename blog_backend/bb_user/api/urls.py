from django.conf.urls import patterns, url

from .views import user


urlpatterns = patterns('',
                       url(r'^$', user.Collection.as_view()),
                       url(r'^/login$', user.UserLogin.as_view()),
                       url(r'^/(?P<user_id>\d+)$', user.Single.as_view()),
                       )
