import cPickle

class JWTMiddleware(object):
    """
    Get user stored in COOKIES.
    """
    authentication_header_prefix = 'Token'

    def process_request(self, request):
        """
        The `process_request` method is called on every request regardless of
        whether the endpoint requires authentication.

        `process_request` has two possible return values:

        1) `None` - We return `None` if we have user data in COOKIES.

        2) `request.user` - We return a user we have user data in COOKIES.
        """
        # get user data (id, username, email, is_confirmed, token)
        user = request.COOKIES.get('user')

        if user:
            request.user = cPickle.loads(user)
