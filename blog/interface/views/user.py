import cPickle
from django.views.generic import DetailView

from interface.mixins import BackendMixin, UserMixin


class View(BackendMixin, UserMixin, DetailView):

    context_object_name = 'blog_user'
    template_name = 'interface/user/view.html'

    def get_object(self, queryset=None):
        return self.user

    def render_to_response(self, context, **response_kwargs):
        response = super(View, self).render_to_response(context, **response_kwargs)
        # todo: implement saving user data to cookie after logination
        # serialized_user = cPickle.dumps({
        #     "username": "voron",
        #     "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZXhwIjoxNTAxMTU1NzUxfQ.p1UqHznb3cmE4BWCX-Ethmgg_HzVe46I6e0j7g0GW_c",
        #     "id": 1,
        #     "is_confirmed": False,
        #     "email": "1@gmail.com"
        # })
        # response.set_cookie('user', serialized_user)

        return response