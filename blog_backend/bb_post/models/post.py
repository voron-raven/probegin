from django.db import models
from django.conf import settings


class Post(models.Model):

    subject = models.TextField()
    content = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta(object):
        app_label = 'bb_post'
        db_table = 'post'
